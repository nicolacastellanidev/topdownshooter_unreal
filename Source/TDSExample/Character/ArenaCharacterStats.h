// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSExample/DataTables/ArenaItemStatsModifier.h"
#include "TDSExample/Models/WeaponType.h"
#include "ArenaCharacterStats.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDSEXAMPLE_API UArenaCharacterStats : public UActorComponent
{
	GENERATED_BODY()
	UArenaCharacterStats();
	/**
	* Called after stats update, applies logic to stats and prevents bad behaviours
	* @author nicola.castellani
	*/
	void SanitizeProperties();
	
public:
	/**
	* Sets target stats value, handles Operation type and Reset after N seconds
	* @param StatName name of the stat to update
	* @param StatValue value to apply
	* @param Operation type of operation
	* @param ResetAfter returns to previous value after
	* @author nicola.castellani
	*/
	UFUNCTION()
	void UpdateStats(const FString& StatName, const float StatValue, const EModifierOperation Operation, const float ResetAfter = 0.0f);
	/**
	* @returns current HP
	* @author nicola.castellani
	*/
	float GetHP() const;
	/**
	* @returns current DEF
	* @author nicola.castellani
	*/
	float GetShields() const;
	/**
	* @returns current FIRERATE
	* @author nicola.castellani
	*/
	float GetFireRate() const;
	/**
	* @returns current WEAPON
	* @author nicola.castellani
	*/
	EWeaponType GetWeapon() const;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStatsUpdated);
	/**
	* Event called after stats updates
	* @author nicola.castellani
	*/
	UPROPERTY(BlueprintAssignable)
	FOnStatsUpdated OnStatsUpdated;
protected:
	UPROPERTY(EditAnywhere, Category="Stats", meta = (ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float HP = 1.0f;
	float LAST_HP = 1.0f;
	UPROPERTY(EditAnywhere, Category="Stats", meta = (ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float DEF = 0.0f;
	UPROPERTY(EditAnywhere, Category="Stats", meta = (ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float FIRERATE = 1.0f;
	UPROPERTY(EditAnywhere, Category="Equipment", meta = (ClampMin="0.0", ClampMax="1.0", UIMin="0.0", UIMax="1.0"))
	float WEAPON = 0;
};
