// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ArenaCharacter.h"
#include "ArenaCharacterStats.h"
#include "Components/ActorComponent.h"
#include "TDSExample/Items/ArenaItem.h"
#include "ArenaCharacterInventory.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDSEXAMPLE_API UArenaCharacterInventory : public UActorComponent
{
	GENERATED_BODY()
	/**
	 * Returns a reference to Character Stats
	 * @author nicola.castellani
	 */
	void GetCharacterStatsComponent();
	virtual void BeginPlay() override;
	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;
	
public:
	UArenaCharacterInventory();
	/**
	* Adds an item and update stats
	* @author nicola.castellani
	*/
	void AddItem(AArenaItem* Item);
	/**
	* @returns the Character Component Stats reference
	* @author nicola.castellani
	*/
	const UArenaCharacterStats* GetComponentStats() const;
	/**
	* @returns current HP
	* @author nicola.castellani
	*/
	float GetHP() const;
	/**
	* @returns current DEF
	* @author nicola.castellani
	*/
	float GetShields() const;
	/**
	* @returns current FIRERATE
	* @author nicola.castellani
	*/
	float GetFireRate() const;
	/**
	* @returns current WEAPON
	* @author nicola.castellani
	*/
	EWeaponType GetWeapon() const;
	
	/**
	* Bindable event for blueprint implementation
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintImplementableEvent)
	void OnItemAdded();

	/**
	* Called after inventory stats updated
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintCallable)
	void AfterStatsUpdated();
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStatsUpdated);
	/**
	* Event called after stats updates
	* @author nicola.castellani
	*/
	UPROPERTY(BlueprintAssignable)
	FOnStatsUpdated OnStatsUpdated;
protected:
	UPROPERTY(Transient)
	UArenaCharacterStats* CharacterStats = nullptr;
};
