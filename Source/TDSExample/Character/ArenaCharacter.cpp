#include "ArenaCharacter.h"

#include "ArenaCharacterInventory.h"
#include "ArenaCharacterStats.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDSExample/Weapon/WeaponArenaNotBP.h"

AArenaCharacter::AArenaCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationYaw = false;

	StatsComponent = CreateDefaultSubobject<UArenaCharacterStats>(TEXT("Stats"));
	InventoryComponent = CreateDefaultSubobject<UArenaCharacterInventory>(TEXT("Inventory"));
}

void AArenaCharacter::AfterStatsUpdate()
{
	CurrentHealth = InventoryComponent->GetHP() * MaxHealth;
	CurrentShields = InventoryComponent->GetShields() * MaxShields;
	
	Weapon->UpdateKind(InventoryComponent->GetWeapon());
	Weapon->UpdateFireRate(InventoryComponent->GetFireRate());

	if (CurrentHealth == 0.0f) Die();

	OnStatsUpdated();
}

void AArenaCharacter::BeginPlay()
{
	Super::BeginPlay();
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();

	PlayerController->bShowMouseCursor = false;
	PlayerController->bEnableClickEvents = true;
	PlayerController->bEnableMouseOverEvents = true;
	SpawnWeapon();
	/**
	* I should change stats management using CharacterStats component directly.
	* But I want to optimize the cost of updates operations to a few operations call, only
	* when stats are really updated, and not at every frame.
	*/
	InventoryComponent->OnStatsUpdated.AddDynamic(this, &AArenaCharacter::AfterStatsUpdate);

	AfterStatsUpdate();
}

void AArenaCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor == nullptr) return;
	const bool bIsItem = OtherActor->IsA<AArenaItem>();

	if (bIsItem)
	{
		AArenaItem* Item = Cast<AArenaItem>(OtherActor);
		InventoryComponent->AddItem(Item);
		Item->Destroy();
	}
}

void AArenaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveUp", this, &AArenaCharacter::MoveUp);
	PlayerInputComponent->BindAxis("MoveRight", this, &AArenaCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookRight", this, &AArenaCharacter::LookRight);
	PlayerInputComponent->BindAxis("LookUp", this, &AArenaCharacter::LookUp);
	PlayerInputComponent->BindAxis("Shoot", this, &AArenaCharacter::Shoot);
	PlayerInputComponent->BindAxis("Spell_A", this, &AArenaCharacter::Sprint);
}

void AArenaCharacter::MoveUp(const float AxisAmount)
{
	if (IsDead()) return;
	AddMovementInput(FVector::ForwardVector * AxisAmount);
}

void AArenaCharacter::MoveRight(const float AxisAmount)
{
	if (IsDead()) return;
	AddMovementInput(FVector::RightVector * AxisAmount);
}

void AArenaCharacter::LookRight(const float AxisAmount)
{
	if (IsDead()) return;
	if (AxisAmount)
	{
		AddControllerYawInput(AxisAmount * 2.f);
	}
}

void AArenaCharacter::LookUp(const float AxisAmount)
{
	if (IsDead()) return;
	if (AxisAmount)
	{
		AddControllerPitchInput(AxisAmount * 2.f);
	}
}

// @todo this method cannot be const due to BindAxis signature, how can i set this method const?
// ReSharper disable once CppMemberFunctionMayBeConst
void AArenaCharacter::Shoot(const float AxisAmount)
{
	if (AxisAmount != 0.0f)
	{
		Weapon->StartFire();
	}
	else
	{
		Weapon->StopFire();
	}
}

void AArenaCharacter::Sprint(const float AxisAmount)
{
	if (!OnSprint && AxisAmount != 0.0f)
	{
		CustomTimeDilation = 5.f;
		OnSprint = true;
		// reset spell after 2s
		GetWorld()->GetTimerManager().SetTimer(SpellATimerHandle, this, &AArenaCharacter::StopSprint, 0.5f, false);
	}
	else
	{
		if (AxisAmount == 0.f)
		{
			// reset
			CustomTimeDilation = 1.f;
			if (OnSprint)
			{
				// cancel timer
				GetWorld()->GetTimerManager().ClearTimer(SpellATimerHandle);
			}
			OnSprint = false;
		}
	}
}


void AArenaCharacter::StopSprint()
{
	CustomTimeDilation = 1.f;
}

void AArenaCharacter::SpawnWeapon()
{
	const FVector Location(0.0f, 0.0f, 0.0f);
	const FRotator Rotation(0.0f, 0.0f, 0.0f);
	Weapon = Cast<AWeaponArenaNotBP>(GetWorld()->SpawnActor<AActor>(WeaponClass, Location, Rotation));
	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "GunSocket");
	Weapon->SetPlayer(*this);
}

const UArenaCharacterInventory* AArenaCharacter::GetInventory() const
{
	return InventoryComponent;
}

void AArenaCharacter::Die()
{
	Dead = true;

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetCollisionProfileName("Ragdoll");
	GetCharacterMovement()->DisableMovement();

	GetWorld()->GetTimerManager().SetTimer(DieTimerHandle, this, &AArenaCharacter::EndGame, 2.0f, false);
}

void AArenaCharacter::EndGame() const
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}