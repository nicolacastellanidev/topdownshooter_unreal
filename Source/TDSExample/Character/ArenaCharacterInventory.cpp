// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacterInventory.h"

#include "TDSExample/DataTables/ArenaItemStatsModifier.h"


void UArenaCharacterInventory::GetCharacterStatsComponent()
{
	AActor* Owner  = GetOwner();
	if(Owner == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] Owner is null"));
		return;
	}
	CharacterStats = Owner->FindComponentByClass<UArenaCharacterStats>();

	if(!CharacterStats)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] Missing CharacterStats component"));
		return;
	}

	CharacterStats->OnStatsUpdated.AddDynamic(this, &UArenaCharacterInventory::AfterStatsUpdated);
}

void UArenaCharacterInventory::BeginPlay()
{
	Super::BeginPlay();

	if(CharacterStats == nullptr)
	{
		GetCharacterStatsComponent();
	}

	if(CharacterStats == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] CharacterStats is null"));
		return;
	}
}

UArenaCharacterInventory::UArenaCharacterInventory()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UArenaCharacterInventory::InitializeComponent()
{
	GetCharacterStatsComponent();
}

void UArenaCharacterInventory::UninitializeComponent()
{
	CharacterStats = nullptr;
	Super::UninitializeComponent();
}

void UArenaCharacterInventory::AddItem(AArenaItem* Item)
{
	if(CharacterStats == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] CharacterStats is null"));
		return;
	}

	if(Item == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] Item is null"));
		return;
	}

	UArenaItemDataAsset* DataAsset = Item->GetData();

	if(DataAsset == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] DataAsset is null"));
		return;
	}

	UE_LOG(LogTemp, Log, TEXT("[UArenaCharacterInventory] Added item %s"), *DataAsset->ItemName.ToString());

	UDataTable* Modifiers = DataAsset->StatsModifier;

	if(Modifiers == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UArenaCharacterInventory] Modifiers is null"));
		return;
	}

	static const FString ContextString(TEXT("UArenaCharacterInventory::AddItem"));

	TArray<FArenaItemStatsModifier*> Rows;

	Modifiers->GetAllRows(ContextString, Rows);

	for (const FArenaItemStatsModifier* Row : Rows)
	{
		CharacterStats->UpdateStats(Row->StatName, Row->Value, Row->Operation, Row->ResetAfter);
	}
}

const UArenaCharacterStats* UArenaCharacterInventory::GetComponentStats() const
{
	return CharacterStats;
}

float UArenaCharacterInventory::GetHP() const
{
	return CharacterStats->GetHP();
}

float UArenaCharacterInventory::GetShields() const
{
	return CharacterStats->GetShields();
}

float UArenaCharacterInventory::GetFireRate() const
{
	return CharacterStats->GetFireRate();
}

EWeaponType UArenaCharacterInventory::GetWeapon() const
{
	return CharacterStats->GetWeapon();
}

void UArenaCharacterInventory::AfterStatsUpdated()
{
	OnStatsUpdated.Broadcast();
}
