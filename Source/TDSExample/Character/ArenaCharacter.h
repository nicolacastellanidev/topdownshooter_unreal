// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
// ReSharper disable once CppUnusedIncludeDirective
#include "GameFramework/CharacterMovementComponent.h"

#include "ArenaCharacter.generated.h"

class UArenaCharacterInventory;
class UArenaCharacterStats;
class AWeaponArenaNotBP;
UCLASS()
class TDSEXAMPLE_API AArenaCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	/**
	 * Sets default values for this character's properties
	 * @author nicola.castellani
	 */
	AArenaCharacter();

	/**
	* Called after stats are updated by an Item (Blueprint Event)
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintImplementableEvent)
	void OnStatsUpdated();
	/**
	* Called after stats are updated by an Item
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintCallable)
	void AfterStatsUpdate();
	/**
	* Returns max available health
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() const { return MaxHealth; }
	/**
	* @return true if player is dead, or is waiting to
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintCallable)
	bool IsDead() const { return Dead; }

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// movement
	void MoveUp(const float AxisAmount);
	void MoveRight(const float AxisAmount);
	void LookRight(const float AxisAmount);
	void LookUp(float AxisAmount);
	
	// shooting / spell casting
	void Shoot(float AxisAmount);
	void Sprint(const float AxisAmount);
	void StopSprint();
	void SpawnWeapon();
	
	/**
	* @return the player inventory reference
	* @author nicola.castellani
	*/
	const UArenaCharacterInventory* GetInventory() const;
	
	/** Weapon to spawn class */
	UPROPERTY( EditAnywhere, Category="Weapon" )
	TSubclassOf<AActor> WeaponClass;

	UPROPERTY( EditAnywhere, BlueprintReadOnly, Category="Health" )
	float CurrentHealth = 100.f;
	
	UPROPERTY( EditAnywhere, BlueprintReadOnly, Category="Health" )
	float CurrentShields = 0.f;
	
	UPROPERTY( EditAnywhere, Category="Health" )
	float MaxHealth = 100.f;

	UPROPERTY( EditAnywhere, Category="Health" )
	float MaxShields = 100.f;
	
	AWeaponArenaNotBP* Weapon;

protected:
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess="true"))
	UArenaCharacterStats* StatsComponent;

	UPROPERTY(Category=Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess="true"))
	UArenaCharacterInventory* InventoryComponent;
private:
	/**
	* Set Dead to true and wait a little bit before destroying the player
	* @author nicola.castellani
	*/
	void Die();
	/**
	* Reload current scene
	* @author nicola.castellani
	*/
	void EndGame() const;
	
	bool Dead = false;
	bool OnSprint = false;

	FTimerHandle DieTimerHandle;
	FTimerHandle SpellATimerHandle;
};
