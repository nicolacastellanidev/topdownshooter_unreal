#include "ArenaCharacterStats.h"
#include "ReflectionUtilsFunctionLibrary.h"
#include "TDSExample/DataTables/ArenaItemStatsModifier.h"

UArenaCharacterStats::UArenaCharacterStats()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UArenaCharacterStats::SanitizeProperties()
{
	if (DEF >= 0.1f)
	{
		HP = 1;
	}
	LAST_HP = HP;
}

void UArenaCharacterStats::UpdateStats(
	const FString& StatName,
	const float StatValue,
	const EModifierOperation Operation,
	const float ResetAfter)
{
	if (GEngine == nullptr) { return; }

	// path of the stat, another example: This.Items[0].HP
	const FString Path = "This." + StatName;
	void* OutObject = nullptr;	
	FProperty* StatProperty = UReflectionUtilsFunctionLibrary::RetrieveProperty(this, Path, OutObject);

	if (StatProperty == nullptr) { return; }

	FFloatProperty* FloatProperty = CastField<FFloatProperty>(StatProperty);
	if (FloatProperty == nullptr) { return; }

	const float Value = FMath::Clamp(StatValue, 0.f, 1.f);
	const float CurrentValue = FloatProperty->GetPropertyValue_InContainer(OutObject);

	if (ResetAfter != 0.0f)
	{
		FTimerDelegate TimerDel;
		FTimerHandle TimerHandle;
		TimerDel.BindUFunction(this, FName("UpdateStats"), StatName, CurrentValue, EModifierOperation::Set, 0.0f);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, 5.f, false);
	}

	switch (Operation)
	{
	case EModifierOperation::Add:
		FloatProperty->SetPropertyValue_InContainer(OutObject, FMath::Clamp(CurrentValue + Value, 0.f, 1.f));
		break;
	case EModifierOperation::Diff:
		FloatProperty->SetPropertyValue_InContainer(OutObject, FMath::Clamp(CurrentValue - Value, 0.f, 1.f));
		break;
	case EModifierOperation::Set:
		FloatProperty->SetPropertyValue_InContainer(OutObject, Value);
		break;
	default: break;
	}

	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Stats Updated:"));
	GEngine->AddOnScreenDebugMessage(-2, 5.0f, FColor::Cyan,
	                                 TEXT("[Name] " + StatName + " [Value] " + FString::SanitizeFloat(StatValue)));
	SanitizeProperties();
	OnStatsUpdated.Broadcast();
}

float UArenaCharacterStats::GetHP() const
{
	return HP;
}

float UArenaCharacterStats::GetShields() const
{
	return DEF;
}

float UArenaCharacterStats::GetFireRate() const
{
	return FIRERATE;
}

EWeaponType UArenaCharacterStats::GetWeapon() const
{
	// @todo not a scalable solution
	if (WEAPON < 1.f)
	{
		return EWeaponType::Rifle;
	}
	return EWeaponType::Shotty;
}