// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaGameMode.h"

void AArenaGameMode::AddEnemy()
{
	if (!CanSpawnEnemy()) return;
	EnemiesCount++;
}

void AArenaGameMode::RemoveEnemy()
{
	EnemiesCount--;
	if(EnemiesCount < 0)
	{
		EnemiesCount = 0;
	}

	CurrentScore += 100;
}

bool AArenaGameMode::CanSpawnEnemy() const
{
	return EnemiesCount + 1 <= MaxEnemies;
}
