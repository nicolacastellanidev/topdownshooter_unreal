// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "TDSExample/Models/WeaponType.h"
#include "ArenaItemStatsModifier.generated.h"

UENUM(BlueprintType)
enum class EModifierOperation: uint8
{
	Set = 0 UMETA(DisplayName="Set"),
	Add = 1 UMETA(DisplayName="Add"),
	Diff = 2 UMETA(DisplayName="Diff"),
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct FArenaItemStatsModifier : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, Category="Item")
	FString StatName;
	
	UPROPERTY(EditAnywhere, Category="Item")
	float Value = 0.0f;

	UPROPERTY(EditAnywhere, Category="Item")
	EModifierOperation Operation = EModifierOperation::Set;
	
	UPROPERTY(EditAnywhere, Category="Item")
    float ResetAfter = 0.0f;
};