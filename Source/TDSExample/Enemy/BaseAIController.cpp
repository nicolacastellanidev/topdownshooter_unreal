// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAIController.h"
#include "BaseEnemy.h"
#include "TDSExample/Character/ArenaCharacter.h"

ABaseAIController::ABaseAIController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseAIController::BeginPlay()
{
	Super::BeginPlay();
	PlayerPawn = Cast<AArenaCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	
	if(PlayerPawn)
	{
		GetWorld()->GetTimerManager().SetTimer(ChaseTimerHandle, this, &ABaseAIController::Chase, 1.0f, true);
	}
}

// Called every frame
void ABaseAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(PlayerPawn->IsDead())
	{
		Pawn->TargetNotInRange();
		return;
	}
	if(bChasing)
	{
		// look at target
		LookAtTarget(DeltaTime);
		if(!Pawn) return;
		if(IsTargetInRange())
		{
			Pawn->TargetInRange();
		} else
		{
			Pawn->TargetNotInRange();
		}
	}
}

bool ABaseAIController::IsTargetInRange() const
{
	EPathFollowingStatus::Type pl = GetMoveStatus();
	return pl == EPathFollowingStatus::Type::Idle;
}

void ABaseAIController::LookAtTarget(float DeltaTime)
{
	SetFocalPoint(PlayerPawn->GetTransform().GetLocation());
	UpdateControlRotation(DeltaTime);
}

void ABaseAIController::Chase()
{
	MoveToActor(PlayerPawn, 500.f);
	bChasing = true;
}

void ABaseAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	Pawn = Cast<ABaseEnemy>(InPawn);
}
