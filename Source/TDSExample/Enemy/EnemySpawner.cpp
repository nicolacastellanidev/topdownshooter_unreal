// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"
#include "TDSExample/ArenaGameMode.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &AEnemySpawner::Spawn, SpawnRate, true);
	MGameMode = GetWorld()->GetAuthGameMode<AArenaGameMode>();
}

void AEnemySpawner::Spawn() const
{
	if(!MGameMode->CanSpawnEnemy())
	{
		return;
	}
	
	// lock
	MGameMode->AddEnemy();
	FVector Location = GetTransform().GetLocation();
	const FQuat Rotation = GetTransform().GetRotation();

	Location.Z += 100.f;
	
	GetWorld()->SpawnActor<AActor>(
		PawnToSpawn,
		Location,
		Rotation.Rotator()
	);
}
