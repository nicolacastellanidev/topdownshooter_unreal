// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseEnemy.h"

#include "TDSExample/ArenaGameMode.h"
#include "TDSExample/Weapon/WeaponArenaNotBP.h"

// Sets default values
ABaseEnemy::ABaseEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
	SpawnWeapon();
	MGameMode = GetWorld()->GetAuthGameMode<AArenaGameMode>();
}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABaseEnemy::SpawnWeapon()
{
	const FVector Location(0.0f, 0.0f, 0.0f);
	const FRotator Rotation(0.0f, 0.0f, 0.0f);
	if (WeaponClass)
	{
		Weapon = Cast<AWeaponArenaNotBP>(GetWorld()->SpawnActor<AActor>(WeaponClass, Location, Rotation));
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, "GunSocket");
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No WeaponClass defined"));
	}
}

void ABaseEnemy::TargetInRange() const
{
	if (Dead)
	{
		return;
	}
	Weapon->StartFire();
}

void ABaseEnemy::TargetNotInRange() const
{
	Weapon->StopFire();
}

void ABaseEnemy::DropLoot()
{

	float Random = 0.0f;
	for (FLootToSpawn Loot : LootsToSpawn)
	{
		Random = FMath::RandRange(0.0f, 1.0f);
		if(Random <= Loot.Probability)
		{
			GetWorld()->SpawnActor<AActor>(
				Loot.ItemToSpawn,
				GetActorLocation() + FVector(FMath::RandRange(1.0f, 100.0f),FMath::RandRange(1.0f, 100.0f),50.0f),
				GetActorRotation()
			);

			break;
		}
	} 
}

void ABaseEnemy::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved,
                           FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Dead) return;
	if (
		OtherComp->ComponentHasTag("EnemyBullet")
		|| OtherComp->ComponentHasTag("PlayerBullet"))
	{
		Dead = true;

		AfterDie();

		MGameMode->RemoveEnemy();

		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		GetMesh()->SetAllBodiesSimulatePhysics(true);
		GetMesh()->SetCollisionProfileName("Ragdoll");

		GetCharacterMovement()->DisableMovement();

		GetWorld()->GetTimerManager().SetTimer(DieTimerHandle, this, &ABaseEnemy::Die, 5.f, false);

		Weapon->StopFire();
		// shoot a projectile randomly
		Weapon->Shoot();

		DropLoot();
	}
}

void ABaseEnemy::Die()
{
	Weapon->Destroy();
	Destroy();
}
