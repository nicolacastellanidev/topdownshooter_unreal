// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
// ReSharper disable once CppUnusedIncludeDirective
#include "GameFramework/CharacterMovementComponent.h"
// ReSharper disable once CppUnusedIncludeDirective
#include "Components/CapsuleComponent.h"
#include "TDSExample/Items/ArenaItem.h"
#include "BaseEnemy.generated.h"

class AArenaGameMode;
class AWeaponArenaNotBP;

USTRUCT(BlueprintType)
struct FLootToSpawn
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Settings")
	TSubclassOf<AArenaItem> ItemToSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Settings")
	float Probability;
};

UCLASS()
class TDSEXAMPLE_API ABaseEnemy : public ACharacter
{
	GENERATED_BODY()

	ABaseEnemy();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void NotifyHit
	(
		class UPrimitiveComponent* MyComp,
		AActor* Other,
		class UPrimitiveComponent* OtherComp,
		bool bSelfMoved,
		FVector HitLocation,
		FVector HitNormal,
		FVector NormalImpulse,
		const FHitResult& Hit
	) override;

public:
	/**
	* Attach weapon to Actor
	* @author nicola.castellani
	*/
	void SpawnWeapon();
	/**
	* Called when target is in range
	* @author nicola.castellani
	*/
	void TargetInRange() const;
	/**
	* Called when target is not in range
	* @author nicola.castellani
	*/
	void TargetNotInRange() const;

	/**
	* Blueprint callback after actor death
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintImplementableEvent)
	void AfterDie();

	UPROPERTY(EditAnywhere, Category="Weapon")
	TSubclassOf<AActor> WeaponClass;

	UPROPERTY(EditAnywhere, Category="Loot")
	TArray<FLootToSpawn> LootsToSpawn;

private:
	/**
	* Drops randomly items to pick up
	* @author nicola.castellani
	*/
	void DropLoot();
	/**
	* Sets dead to true and do other pre-death stuff
	* @author nicola.castellani
	*/
	void Die();

	FTimerHandle DieTimerHandle;
	AWeaponArenaNotBP* Weapon;
	AArenaGameMode* MGameMode;
	
	bool Dead = false;
};
