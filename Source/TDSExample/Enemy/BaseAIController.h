// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseEnemy.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BaseAIController.generated.h"

class AArenaCharacter;
/**
 * 
 */
UCLASS()
class TDSEXAMPLE_API ABaseAIController : public AAIController
{
	GENERATED_BODY()

	ABaseAIController();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
public:
	/**
	* @returns true if IA target is in range
	* @author nicola.castellani
	*/
	bool IsTargetInRange() const;
	/**
	* Rotates the actor to look at target
	* @author nicola.castellani
	*/
	void LookAtTarget(float DeltaTime);
	/**
	* Start attack
	* @author nicola.castellani
	*/
	void Chase();

protected:
	virtual void OnPossess(APawn* InPawn) override;
	FTimerHandle ChaseTimerHandle;
	AArenaCharacter* PlayerPawn;
	UCharacterMovementComponent* CharacterMovement;
	bool bChasing = false;
	
private:
	ABaseEnemy* Pawn;
};
