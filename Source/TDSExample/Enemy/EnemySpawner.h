// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BaseAIController.h"
#include "BaseEnemy.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

class AArenaGameMode;
UCLASS()
class TDSEXAMPLE_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	AEnemySpawner();
	virtual void BeginPlay() override;


public:
	/**
	* Spawns enemy
	* @author nicola.castellani
	*/
	void Spawn() const;

protected:
	UPROPERTY(EditAnywhere, Category = "Spawn")
	float SpawnRate = 5.f;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSubclassOf<ABaseEnemy> PawnToSpawn;

private:
	FTimerHandle SpawnTimerHandle;
	AArenaGameMode* MGameMode;
};
