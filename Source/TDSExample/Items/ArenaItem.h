// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSExample/DataAssets/ArenaItemDataAsset.h"
#include "ArenaItem.generated.h"

UCLASS()
class TDSEXAMPLE_API AArenaItem : public AActor
{
	GENERATED_BODY()
	AArenaItem();
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

public:
	/**
	* @returns ItemDataAsset
	* @author nicola.castellani
	*/
	UArenaItemDataAsset* GetData() const;

protected:
	UPROPERTY(EditAnywhere, Category="Configuration")
	bool AutoDestroy = true;

	UPROPERTY(EditAnywhere, Category="Item Data")
	UArenaItemDataAsset* ItemDataAsset = nullptr;

	UPROPERTY(BlueprintReadOnly)
	bool AutoDestroyed = false;

private:
	/**
	* Called when lifetime is expired
	* @author nicola.castellani
	*/
	void OnDestroy();
	FTimerHandle AutoDestroyTimerHandle;
};
