// Fill out your copyright notice in the Description page of Project Settings.
#include "ArenaItem.h"

void AArenaItem::BeginPlay()
{
	Super::BeginPlay();
	if (!AutoDestroy) return;
	GetWorld()->GetTimerManager().SetTimer(AutoDestroyTimerHandle, this, &AArenaItem::OnDestroy, 5.0f, true);
}

void AArenaItem::BeginDestroy()
{
	Super::BeginDestroy();
}

UArenaItemDataAsset* AArenaItem::GetData() const
{
	return ItemDataAsset;
}

// Sets default values
AArenaItem::AArenaItem()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AArenaItem::OnDestroy()
{
	AutoDestroyed = true;
	Destroy();
}
