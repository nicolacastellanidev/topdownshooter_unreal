// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponArenaNotBP.h"

#include "TDSExample/Character/ArenaCharacterInventory.h"

// Sets default values
AWeaponArenaNotBP::AWeaponArenaNotBP()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	UE_LOG(LogTemp, Warning, TEXT("[Weapon] - Created"));

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Root Mesh"));
	RootComponent = MeshComponent;

	GunDirectionComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("Gun Direction"));
	GunDirectionComponent->SetupAttachment(MeshComponent);
	GunDirectionComponent->ArrowSize = .5f;

	const FVector GunDirCompLocation = FVector(0.f, 50.f, 10.f);
	const FRotator GunDirCompRotation = FVector(0.f, 0.f, 90.f).Rotation();
	GunDirectionComponent->SetRelativeLocationAndRotation(GunDirCompLocation, GunDirCompRotation);

	CurrentFireRate = M_FireRate;
}

void AWeaponArenaNotBP::StartFire()
{
	if (Firing == 1 || OnCooldown) return;
	Firing = 1;
	StopFire();
	Shoot();
	GetWorld()->GetTimerManager().SetTimer(ShootTimerHandle, this, &AWeaponArenaNotBP::Shoot, CurrentFireRate, true);
}

void AWeaponArenaNotBP::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(ShootTimerHandle);
	Firing = 0;
}

void AWeaponArenaNotBP::UpdateFireRate(float NextFireRate)
{
	M_FireRate = CurrentWeaponType == EWeaponType::Rifle ? RifleFireRate : ShottyFireRate;
	if (CurrentFireRate == M_FireRate * NextFireRate)
	{
		// prevent multiple firerate change with the same value
		return;
	}
	CurrentFireRate = M_FireRate * NextFireRate;

	if (Firing == 1)
	{
		GetWorld()->GetTimerManager().ClearTimer(CooldownResetHandle);
		StopFire();
		StartFire();
	}
}

void AWeaponArenaNotBP::UpdateKind(EWeaponType WeaponType)
{
	if (WeaponType == CurrentWeaponType) return;
	CurrentWeaponType = WeaponType;
}

void AWeaponArenaNotBP::ResetCooldown()
{
	OnCooldown = false;
}

void AWeaponArenaNotBP::RifleShoot() const
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	const FTransform t = GunDirectionComponent->GetComponentTransform();
	ABaseProjectile* Projectile = GetWorld()->SpawnActor<ABaseProjectile>(
		ProjectileClass,
		t.GetLocation(),
		t.Rotator(),
		SpawnParams
	);
	if (Projectile)
	{
		// Set the projectile's initial trajectory.
		const FVector LaunchDirection = t.Rotator().Vector();
		Projectile->FireInDirection(LaunchDirection);
	}
}

void AWeaponArenaNotBP::ShottyShoot() const
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	const FTransform t = GunDirectionComponent->GetComponentTransform();
	FRandomStream RandStream;

	for (int i = -2; i <= 2; ++i)
	{
		FRotator Rotation = t.Rotator();
		ABaseProjectile* Projectile = GetWorld()->SpawnActor<ABaseProjectile>(
			ProjectileClass,
			t.GetLocation(),
			Rotation,
			SpawnParams
		);
		if (Projectile)
		{
			RandStream.GenerateNewSeed();
			// Set the projectile's initial trajectory.
			Rotation.Yaw += 5.0f * i;
			const FVector LaunchDirection = Rotation.Vector();
			Projectile->FireInDirection(LaunchDirection);
		}
	}
}

void AWeaponArenaNotBP::Shoot()
{
	if (OnCooldown) return;
	OnCooldown = true;
	GetWorld()->GetTimerManager().SetTimer(CooldownResetHandle, this, &AWeaponArenaNotBP::ResetCooldown,
	                                       CurrentFireRate);
	switch (CurrentWeaponType)
	{
	case EWeaponType::Rifle:
		RifleShoot();
		break;
	case EWeaponType::Shotty:
		ShottyShoot();
		break;
	default: ;
	}

	AfterShoot();
}

void AWeaponArenaNotBP::SetPlayer(const AArenaCharacter& _Player)
{
	this->Player = &_Player;
}

// Called when the game starts or when spawned
void AWeaponArenaNotBP::BeginPlay()
{
	Super::BeginPlay();
	M_FireRate = CurrentWeaponType == EWeaponType::Rifle ? RifleFireRate : ShottyFireRate;
	CurrentFireRate = M_FireRate;
}
