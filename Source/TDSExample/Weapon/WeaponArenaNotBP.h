// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "TDSExample/Bullet/BaseProjectile.h"
#include "TDSExample/Character/ArenaCharacter.h"
#include "WeaponArenaNotBP.generated.h"

enum class EWeaponType : uint8;
UCLASS()
class TDSEXAMPLE_API AWeaponArenaNotBP : public AActor
{
	GENERATED_BODY()
	AWeaponArenaNotBP();
	virtual void BeginPlay() override;
public:
	/**
	* Start to fire
	* @author nicola.castellani
	*/
	void StartFire();
	/**
	* Stop fire
	* @author nicola.castellani
	*/
	void StopFire();
	/**
	* Reset firing state and update fire rate
	* @author nicola.castellani
	*/
	void UpdateFireRate(float NextFireRate);
	/**
	* Updates weapon type, and reset firing if weapon is shooting
	* @author nicola.castellani
	*/
	void UpdateKind(EWeaponType WeaponType);
	/**
	* Reset the OnCooldown to false
	* @author nicola.castellani
	*/
	void ResetCooldown();
	/**
	* Rifle kind shoot
	* @author nicola.castellani
	*/
	void RifleShoot() const;
	/**
	* Shotty kind shoot
	* @author nicola.castellani
	*/
	void ShottyShoot() const;
	
	/**
	* Shoot depending on weapon kind
	* @author nicola.castellani
	*/
	UFUNCTION()
	void Shoot();
	/**
	* Callback after shoot
	* @author nicola.castellani
	*/
	UFUNCTION(BlueprintImplementableEvent)
	void AfterShoot();
	/**
	* Set the player reference
	* @author nicola.castellani
	*/
	void SetPlayer(const AArenaCharacter& _Player);
protected:
	// PROPERTIES
	UPROPERTY(EditAnywhere, Category = "Weapon")
	float RifleFireRate = 0.25f;
	UPROPERTY(EditAnywhere, Category = "Weapon")
	float ShottyFireRate = 1.0f;
	float M_FireRate = 0.25f;

	UPROPERTY(EditAnywhere, Category = "Projectile")
	TSubclassOf<ABaseProjectile> ProjectileClass;

	UPROPERTY(VisibleAnywhere, Category= "Components")
	USkeletalMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, Category= "Components")
	UArrowComponent* GunDirectionComponent;

	UPROPERTY(BlueprintReadOnly)
	EWeaponType CurrentWeaponType;

private:
	FTimerHandle ShootTimerHandle;
	FTimerHandle CooldownResetHandle;
	const AArenaCharacter* Player;
	float CurrentFireRate;
	char Firing = 0;
	bool OnCooldown = false;
};
