// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArenaGameMode.generated.h"

/**
 *
 */
UCLASS()
class TDSEXAMPLE_API AArenaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	int GetMaxEnemies() const { return MaxEnemies; }

	UFUNCTION(BlueprintCallable)
	int GetCurrentEnemies() const { return EnemiesCount; }

	UFUNCTION(BlueprintCallable)
	int GetCurrentScore() const { return CurrentScore; }

	void AddEnemy();
	void RemoveEnemy();
	bool CanSpawnEnemy() const;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = GameSettings)
	int MaxEnemies = 200;

	int EnemiesCount = 0;
	int CurrentScore = 0;
};
