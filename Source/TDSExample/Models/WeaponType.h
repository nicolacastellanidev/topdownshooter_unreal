﻿#pragma once

UENUM(BlueprintType)
enum class EWeaponType: uint8
{
	Rifle = 0 UMETA(DisplayName = "Rifle"),
	Shotty = 1 UMETA(DisplayName = "Shotty"),
};