// Fill out your copyright notice in the Description page of Project Settings.


#include "ReflectionUtilsFunctionLibrary.h"

FProperty* UReflectionUtilsFunctionLibrary::RetrieveProperty(UObject* InObject, const FString& InPath, void*& OutTarget)
{
	if (InObject == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UReflectionUtilsFunctionLibrary] InObject is null"));
		return nullptr;
	}

	FString ObjectString;
	FString PropertyString;

	const bool bSucceeded = InPath.Split(".", &ObjectString, &PropertyString);

	if (!bSucceeded)
	{
		UE_LOG(LogTemp, Error, TEXT("[UReflectionUtilsFunctionLibrary] Malformed path"));
		return nullptr;
	}

	UObject* TargetObject = nullptr;

	if (ObjectString.Equals("This", ESearchCase::IgnoreCase))
	{
		TargetObject = InObject;
	}
	else
	{
		AActor* InActor = Cast<AActor>(InObject);

		if (InActor == nullptr)
		{
			UE_LOG(LogTemp, Error, TEXT("[UReflectionUtilsFunctionLibrary] InActor is null"));
			return nullptr;
		}

		TArray<UActorComponent*> Components;
		InActor->GetComponents(Components);

		for (UActorComponent* Component : Components)
		{
			if (!ObjectString.Equals(Component->GetName())) continue;
			TargetObject = Component;
			break;
		}
	}

	if (TargetObject == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[UReflectionUtilsFunctionLibrary] TargetObject is null"));
		return nullptr;
	}

	OutTarget = TargetObject;

	const UClass* TargetClass = TargetObject->GetClass();
	const FName PropertyName = FName(*PropertyString);
	FProperty* OutProperty = TargetClass->FindPropertyByName(PropertyName);

	return OutProperty;
}
