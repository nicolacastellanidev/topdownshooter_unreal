# High-Level Programming Final Exam

Made by Nicola Castellani (**VR361872**), for the Master Game Dev High-Level Programming course, *University of Verona*.

## Goal of the project

Create a ***custom reflection system*** plugin to handle different **player properties**.

## How to play

Download the source code and open in Unreal (***v4.26***), then run the game.
Use the *Tutorial* map to check how items works.

Use both keyboard/mouse or joystick.

### Keyboard/Mouse commands

- WASD to move
- LMB to shoot
- RMB to sprint
- Mouse to look

### JoyStick command

- L3 to move
- R3 to rotate
- L2 to sprint
- R2 to shoot

## Implementation

The *Top-Down Shooter* (aka ***TDS***) project is a simple arcade game, where the player must survive
as long as possible against hordes of enemies.

The reflection system handles different player properties:

* **HP**: adds/removes or refill player HP
* **DEF**: adds/removes or refill player shields
* **FIRE RATE**: boosts weapons fire rate for a little bit
* **WEAPON**: switches between 2 weapons, Rifle and Shotty.

These *power-ups* are available in different forms:

* **lootable**: dropped after killing an enemy
* **gameplay objects**: e.g. enemy bullets, which removes HP or DEF to player

## Technical

### Classes

| Name                         | Description |
| ----------------------------- | ----------- |
| ``ArenaCharacter.h``          | Handles player logic and behaviours |
| ``ArenaCharacterInventory.h`` | Keeps player items, and handles add/removal |
| ``ArenaCharacterStats.h``     | Keeps and updates player stats, using reflection system |
| ``ArenaItem.h``               | An Item can change player stats, using a specific DataTable (**UArenaItemDataAsset**) to define which stat to change |

### Plugins/***ReflectionUtils***

This custom plugin contains a custom reflection system, with the goal of retrieving an object property,
without any information about it.

The core method is 

```c++
static FProperty* RetrieveProperty(UObject* InObject, const FString& InPath, void*& OutTarget);
```

This function takes 3 parameters:

- ***InObject***: the object used to get a specific property.
- ***InPath***: the path of the property (e.g. this.CharacterStats.HP)
- ***OutTarget***: the output to set